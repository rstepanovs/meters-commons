package lv.romans.meters.commons.messaging

import lv.romans.meters.commons.UserRole
import java.util.*

data class NewUserEmail(val email: String, val tempPassword: String)
data class NewUser(val email: String, val role: UserRole)
data class BillUploaded(val fileName: String, val apartmentId: Long, val uploadDate: Date)