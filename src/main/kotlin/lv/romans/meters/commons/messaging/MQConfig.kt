package lv.romans.meters.commons.messaging

object Exchanges {
    const val USER_CREATION = "user.creation"
}

object Queues {
    const val USER_EMAIL = "user_email"
}