package lv.romans.meters.commons.services

import lv.romans.meters.commons.Address

data class PropertyList(val properties: List<Property>)

data class Property(val id: Long, val address: Address, val managerEmail: String)

data class CanUserDownloadBill(val billName: String, val userEmail: String)

