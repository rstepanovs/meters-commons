package lv.romans.meters.commons

object Constants {
    const val HEADER_AUTHORIZATION = "Authorization"
    const val AUTH_PREFIX = "Bearer "
    const val CLAIM_ROLE = "role"
    const val PASSWORD_CHANGE_REQUIRED = "passwordChange"

    const val HEADER_USER_ID="x-user-id"
    const val HEADER_USER_ROLES="x-user-roles"

    const val LOG_CORRELATION_ID="x-request-id"
    const val LOG_USER_ID="x-user-id"
}

enum class UserRole {
    ROLE_USER,
    ROLE_MANAGER,
    ROLE_ADMIN
}