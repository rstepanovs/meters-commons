package lv.romans.meters.commons

data class ApartmentAddress(val city: String, val street: String, val streetNum: Int, val apartment: Int)

data class Address(val city: String, val street: String, val streetNum: Int)