package lv.romans.meters.commons.api

import lv.romans.meters.commons.Address
import java.util.*

data class OwnerSummary(val name: String, val apartments: List<ApartmentInfo>)

data class UserInfo(val name: String, val email: String, val phone: String?)

data class ApartmentDetails(val address: Address, val manager: ManagerInfo, val warnings: List<Warning>,
                            val canSubmit: Boolean, val meters: List<MeterInfo>)

data class ApartmentBills(val bills: List<Bill>)

data class BillingHistory(val bills: List<ApartmentBills>)

data class MeterDetails(val id: Long, val regNr: String, val verificationDate: Date, val readings: List<MeterReading>)

data class ManagerDashboard(val properties: List<PropertySummary>)

data class PropertySetup(val submitDates: Pair<Int, Int>?, val billDates: Pair<Int, Int>?, val apartments: List<PropertySetupApartment>)

data class PropertyDetails(val debt: Float, val apartments: List<PropertyApartmentStatus>)

data class ApartmentState(val verified: Boolean, val address: Address, val owner: OwnerInfo, val canSubmitBill: Boolean, val bills: List<Bill>, val meters: List<NewMeterReading>)

data class AdminDashboard(val properties: List<AdminProperty>)

data class ManagerList(val managers: List<ManagerInfo>)

data class PropertySummary(val id: Long, val address: Address, val isSetUp: Boolean)

data class Bill(val billId: String, val amount: String, val date: Date, val reference: String, val status: BillStatus)

data class ApartmentInfo(val address: Address, val hasWarnings: Boolean)

data class ManagerInfo(val name: String, val email: String, val phoneNumber: String)

data class Warning(val text: String?, val type: WarningType)

data class MeterInfo(val id: Long, val label: String, val regNr: String, val currentValue: Float)

data class NewMeterReading(val label: String, val regNr: String, val currentValue: Float, val previousValue: Float, val entryDate: Date)

data class MeterReading(val date: Date, val value: Float)

data class PropertySetupApartment(val id: Long, val number: Int)

data class OwnerInfo(val name: String, val email: String, val phoneNumber: String?)

data class PropertyApartmentStatus(val id: Long, val number: Int, val status: ApartmentStatus?)

data class AdminProperty(val id: Long, val address: Address, val managerEmail: String)



enum class WarningType {
    ANNOUNCEMENT, LATE_PAYMENT
}

enum class ApartmentStatus {
    UNVERIFIED, UNPAID, PAID
}

enum class BillStatus {
    UNVERIFIED, UNPAID, PAID
}