package lv.romans.meters.commons.api

import lv.romans.meters.commons.Address
import java.util.*

data class Login(val email: String, val password: String)

data class ChangePassword(val newPassword: String, val newPasswordRepeated: String)

data class Readings(val readings: List<MeterReading>)

data class Reading(val id: Long, val amount: Float)

data class PropertyForm(val submitDates: Pair<Int, Int>, val billDates: Pair<Int, Int>)

data class ApartmentForm(val id: Long?, val number: Int, val owner: OwnerInfo, val meters: List<ApartmentSetupMeter>)

data class Notification(val text: String, val activeFrom: Date, val activeUntil: Date)

data class NewProperty(val address: Address, val managerEmail: String)

data class NewManager(val name: String, val email: String, val phoneNumber: String)

data class ApartmentSetupMeter(val name: String, val regNr: String, val reading: Float, val verificationDate: Date, val unit: String = "m3")
